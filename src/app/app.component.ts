import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'project-zero';
    bigSquare;
    bigSquareWstart: number;
    bigSquareEdgeXstart: number;
    inX: number;
    inY: number;
    bigSquareHstart: number;
    bigSquareEdgeYstart: number;

    constructor () {}

    /**
     * Define initial coordinates
     * @param event 
     */
    getCoord(event) {
        this.bigSquareWstart = this.bigSquare.offsetWidth;
        this.bigSquareHstart = this.bigSquare.offsetHeight;
        this.bigSquareEdgeXstart = this.bigSquare.offsetLeft;
        this.bigSquareEdgeYstart = this.bigSquare.offsetTop;
            }

     /**
      * Define resizeLeftTopCorner method
      * @param event 
      */
     resizeLeftTop(event) {
            this.bigSquare.onmousedown = (event_mdn) => {
                this.getCoord(event_mdn);
                this.inX = event_mdn.pageX;
                this.inY = event_mdn.pageY;
                document.onmousemove = (event_mmv) => {
                    let outX = event_mmv.pageX;
                    let deltaX = outX - this.inX;
                    this.bigSquare.style.left = (this.bigSquareEdgeXstart + deltaX) + 'px';
                    this.bigSquare.style.width = (this.bigSquareWstart - deltaX * 2) + 'px';
                    let outY = event_mmv.pageY;
                    let deltaY = outY - this.inY;
                    this.bigSquare.style.top = (this.bigSquareEdgeYstart + deltaY) + 'px';
                    this.bigSquare.style.height = (this.bigSquareHstart - deltaY * 2) + 'px';
                };
                this.buttonUp(event);
            };
        }
/**
 * Define resizeRightTopCorner method
 * @param event 
 */
     resizeRightTop(event) {
            this.bigSquare.onmousedown = (event_mdn) => {
                this.getCoord(event_mdn);
                this.inX = event_mdn.pageX;
                this.inY = event_mdn.pageY;
                document.onmousemove = (event_mmv) => {
                    let outX = event_mmv.pageX;
                    let deltaX = outX - this.inX;
                    this.bigSquare.style.left = (this.bigSquareEdgeXstart - deltaX) + 'px';
                    this.bigSquare.style.width = (this.bigSquareWstart + deltaX * 2) + 'px';
                    let outY = event_mmv.pageY;
                    let deltaY = outY - this.inY;
                    this.bigSquare.style.top = (this.bigSquareEdgeYstart + deltaY) + 'px';
                    this.bigSquare.style.height = (this.bigSquareHstart - deltaY * 2) + 'px';
                };
                this.buttonUp(event);
            };
        }
/**
 * Define resizeLeftBottomCorner method
 * @param event 
 */
     resizeLeftBottom(event) {

            this.bigSquare.onmousedown = (event_mdn) => {
                this.getCoord(event_mdn);
                this.inY = event_mdn.pageY;
                this.inX = event_mdn.pageX;
                document.onmousemove = (event_mmv) =>  {
                    let outY = event_mmv.pageY;
                    let deltaY = outY - this.inY;
                    this.bigSquare.style.top = (this.bigSquareEdgeYstart - deltaY) + 'px';
                    this.bigSquare.style.height = (this.bigSquareHstart + deltaY * 2) + 'px';
                    let outX = event_mmv.pageX;
                    let deltaX = outX - this.inX;
                    this.bigSquare.style.left = (this.bigSquareEdgeXstart + deltaX) + 'px';
                    this.bigSquare.style.width = (this.bigSquareWstart - deltaX * 2) + 'px';
                };
                this.buttonUp(event);
            };

        }
/**
 * Define resizeRightBottomCorner method
 * @param event 
 */
     resizeRightBottom(event) {

            this.bigSquare.onmousedown = (event_mdn) => {
                this.getCoord(event_mdn);
                this.inX = event_mdn.pageX;
                this.inY = event_mdn.pageY;
                document.onmousemove = (event_mmv) => {
                    let outX = event_mmv.pageX;
                    let deltaX = outX - this.inX;
                    this.bigSquare.style.left = (this.bigSquareEdgeXstart - deltaX) + 'px';
                    this.bigSquare.style.width = (this.bigSquareWstart + deltaX * 2) + 'px';
                    let outY = event_mmv.pageY;
                    let deltaY = outY - this.inY;
                    this.bigSquare.style.top = (this.bigSquareEdgeYstart - deltaY) + 'px';
                    this.bigSquare.style.height = (this.bigSquareHstart + deltaY * 2) + 'px';
                };
                this.buttonUp(event);
            };

        }
/**
 * Define resizeRightSide method
 * @param event 
 */
     resizeW(event) {
            this.bigSquare.onmousedown = (event_mdn) =>  {
                this.getCoord(event_mdn);
                this.inX = event_mdn.pageX;
                document.onmousemove = (event_mmv) => {
                    let outX = event_mmv.pageX;
                    let deltaX = outX - this.inX;
                    this.bigSquare.style.left = (this.bigSquareEdgeXstart - deltaX) + 'px';
                    this.bigSquare.style.width = (this.bigSquareWstart + deltaX * 2) + 'px';
                };
                this.buttonUp(event);
            };
        }

/**
 * Define resizeBottomSide method
 * @param event 
 */
     resizeH(event) {

            this.bigSquare.onmousedown = (event_mdn) =>  {
                this.getCoord(event_mdn);
                this.inY = event_mdn.pageY;
                document.onmousemove = (event_mmv) => {
                    let outY = event_mmv.pageY;
                    let deltaY = outY - this.inY;
                    this.bigSquare.style.top = (this.bigSquareEdgeYstart - deltaY) + 'px';
                    this.bigSquare.style.height = (this.bigSquareHstart + deltaY * 2) + 'px';
                };
                this.buttonUp(event);
            };
        }
/**
 * define resizeLeftSide method
 * @param event 
 */

     resizeWChangeOfsettLeft(event) {

            this.bigSquare.onmousedown = (event_mdn) => {
                this.getCoord(event_mdn);
                this.inX = event_mdn.pageX;
                document.onmousemove = (event_mmv) => {
                    let outX = event_mmv.pageX;
                    let deltaX = outX - this.inX;
                    this.bigSquare.style.left = (this.bigSquareEdgeXstart + deltaX) + 'px';
                    this.bigSquare.style.width = (this.bigSquareWstart - deltaX * 2) + 'px';
                };
                this.buttonUp(event);
            };
        }

        /**
         * Define resizeTopSide method
         * @param event 
         */
     resizeHChangeOfsettTop(event) {

        this.bigSquare.onmousedown = (event_mdn) => {
                this.getCoord(event_mdn);
                this.inY = event_mdn.pageY;
                document.onmousemove = (event_mmv) => {
                    let outY = event_mmv.pageY;
                    let deltaY = outY - this.inY;
                    this.bigSquare.style.top = (this.bigSquareEdgeYstart + deltaY) + 'px';
                    this.bigSquare.style.height = (this.bigSquareHstart - deltaY * 2) + 'px';
                };
                this.buttonUp(event);
            };
        }

    /**
     * Define move method
     * @param event 
     */
     squareMove(event) {

            this.bigSquare.onmousedown = (event_mdn) => {
                this.getCoord(event_mdn);
                this.inX = event_mdn.pageX;
                this.inY = event_mdn.pageY;
                document.onmousemove = (event_mmv) => {
                    this.bigSquare.style.left = ((event_mmv.pageX - this.inX) + this.bigSquareEdgeXstart) + 'px';
                    this.bigSquare.style.top = ((event_mmv.pageY - this.inY) + this.bigSquareEdgeYstart) + 'px';
                };
                this.buttonUp(event);
            };
        }

     /**
      * Handle the event mouseup
      * @param event 
      */

     buttonUp(event)  {
            document.onmouseup = () => {
                document.onmousemove = null;
                this.bigSquare.onmouseup = null;
            };
        }

  ngOnInit() {

    this.bigSquare = document.getElementById('bigSquare');

    /**
     * Determine the current position of the cursor
     * Сhange the cursor view
     * Call the appropriate method
     * 
     */

    this.bigSquare.onmousemove = (e) => {

        switch (true) {
                // left-top
                case (e.offsetX <= 15) && (e.offsetY <= 15):
                    this.bigSquare.style.cursor = 'se-resize';
                    this.resizeLeftTop(e);
                    break;
                    // right-top
                case (e.offsetX >= this.bigSquare.offsetWidth - 15) && (e.offsetY <= 15):
                    this.bigSquare.style.cursor = 'sw-resize';
                    this.resizeRightTop(e);
                    break;
                    // left-bottom
                case (e.offsetX <= 15) && (e.offsetY >= this.bigSquare.offsetHeight - 15):
                    this.bigSquare.style.cursor = 'ne-resize';
                    this.resizeLeftBottom(e);
                    break;
                    // right-bottom
                case (e.offsetX >= this.bigSquare.offsetWidth - 15) && (e.offsetY >= this.bigSquare.offsetHeight - 15):
                    this.bigSquare.style.cursor = 'nw-resize';
                    this.resizeRightBottom(e);
                    break;
                    // left
                case (e.offsetX < 10):
                    this.bigSquare.style.cursor = 'e-resize';
                    this.resizeWChangeOfsettLeft(e);
                    break;
                    // right
                case (e.offsetX > this.bigSquare.offsetWidth - 10):
                    this.bigSquare.style.cursor = 'w-resize';
                    this.resizeW(e);
                    break;
                    // bottom
                case (e.offsetY > this.bigSquare.offsetHeight - 10):
                    this.bigSquare.style.cursor = 'n-resize';
                    this.resizeH(e);
                    break;
                    // top
                case (e.offsetY < 10):
                    this.bigSquare.style.cursor = 's-resize';
                    this.resizeHChangeOfsettTop(e);
                    break;
                default:
                    this.bigSquare.style.cursor = 'move';
                    this.squareMove(e);
                    break;
            }
        };
    }
}
